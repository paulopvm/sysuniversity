#include "disciplinalib.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void inserirDisciplina(NodeDisciplina *head, char nome[], int codigo)
{
    NodeDisciplina *currentDisciplina = head;
    while (currentDisciplina->proximo != NULL)
    {
        currentDisciplina = currentDisciplina->proximo;
    }

    currentDisciplina->proximo = (NodeDisciplina *)malloc(sizeof(NodeDisciplina));
    strcpy(currentDisciplina->proximo->disciplina.nome, nome);
    currentDisciplina->proximo->disciplina.codigo = codigo;
    for (int i = 0; i < 50; i++)
    {
        currentDisciplina->proximo->professores[i].codigo = -1;
        currentDisciplina->proximo->alunos[i].ra = -1;
    }
    currentDisciplina->proximo->proximo = NULL;
}

void exibirDisciplinas(NodeDisciplina *head)
{
    NodeDisciplina *currentDisciplina = head;

    while (currentDisciplina != NULL)
    {
        currentDisciplina = currentDisciplina->proximo;
        if (currentDisciplina != NULL)
        {
            printf("NOME: %s CÓDIGO: %d\n", currentDisciplina->disciplina.nome, currentDisciplina->disciplina.codigo);
            printf("----------\n");
            fflush(stdin);
        }
    }
}
bool existeDisciplina(int idDisciplina, NodeDisciplina *head)
{

    NodeDisciplina *currentDisciplina = head;

    while (currentDisciplina != NULL)
    {
        if (currentDisciplina->disciplina.codigo == idDisciplina)
        {
            return true;
        }
        currentDisciplina = currentDisciplina->proximo;
    }
    printf("\tDisciplina não existente\n");
    return false;
}

char *buscaNomeDisciplinaEVincularAluno(NodeDisciplina *head, int idDisciplina, char nome[], int codigo)
{
    NodeDisciplina *currentDisciplina = head;

    while (currentDisciplina != NULL)
    {
        if (currentDisciplina->disciplina.codigo == idDisciplina)
        {

            for (int i = 0; i < 50; i++)
            {
                if (currentDisciplina->alunos[i].ra == -1)
                {
                    strcpy(currentDisciplina->alunos[i].nome, nome);
                    currentDisciplina->alunos[i].ra = codigo;
                    break;
                }
            }

            return currentDisciplina->disciplina.nome;
            break;
        }
        currentDisciplina = currentDisciplina->proximo;
    }

    return "Indisponível";
}

char *buscaNomeDisciplinaEVincularProfessor(NodeDisciplina *head, int idDisciplina, char nome[], int codigo)
{
    NodeDisciplina *currentDisciplina = head;

    while (currentDisciplina != NULL)
    {
        if (currentDisciplina->disciplina.codigo == idDisciplina)
        {
            for (int i = 0; i < 50; i++)
            {
                if (currentDisciplina->professores[i].codigo == -1)
                {
                    strcpy(currentDisciplina->professores[i].nome, nome);
                    currentDisciplina->professores[i].codigo = codigo;
                    break;
                }
            }
            return currentDisciplina->disciplina.nome;
            break;
        }
        currentDisciplina = currentDisciplina->proximo;
    }

    return "Indisponível";
}

void exibirAlunosDeUmaDisciplina(NodeDisciplina *head, int idDisciplina)
{

    if (existeDisciplina(idDisciplina, head))
    {
        NodeDisciplina *currentDisciplina = head;

        while (currentDisciplina != NULL)
        {
            if (currentDisciplina->disciplina.codigo == idDisciplina)
            {
                printf("CODIGO: %d    NOME: %s", currentDisciplina->disciplina.codigo, currentDisciplina->disciplina.nome);
                printf("----ALUNOS INSCRITOS----\n");
                for (int i = 0; i < 50; i++)
                {
                    if (currentDisciplina->alunos[i].ra != -1)
                    {
                        printf("RA: %d     NOME: %s", currentDisciplina->alunos[i].ra, currentDisciplina->alunos[i].nome);
                    }
                }
            }
            currentDisciplina = currentDisciplina->proximo;
        }
    }
}

void exibirProfessoresDeUmaDisciplina(NodeDisciplina *head, int idDisciplina)
{

    if (existeDisciplina(idDisciplina, head))
    {
        NodeDisciplina *currentDisciplina = head;

        while (currentDisciplina != NULL)
        {
            if (currentDisciplina->disciplina.codigo == idDisciplina)
            {
                printf("CODIGO: %d    NOME: %s", currentDisciplina->disciplina.codigo, currentDisciplina->disciplina.nome);
                printf("----PROFESSORES INSCRITOS----\n");
                for (int i = 0; i < 50; i++)
                {
                    if (currentDisciplina->professores[i].codigo != -1)
                    {
                        printf("CODIGO: %d     NOME: %s", currentDisciplina->professores[i].codigo, currentDisciplina->professores[i].nome);
                    }
                }
            }
            currentDisciplina = currentDisciplina->proximo;
        }
    }
}