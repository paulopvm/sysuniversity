#ifndef DISCIPLINA_H_INCLUDED
#define DISCIPLINA_H_INCLUDED
/* ^^ these are the include guards */
#include <stdbool.h>


typedef struct informacoes_de_aluno_cadastrado
{
    char nome[20];
    int ra;
} AlunoCadastrado;

typedef struct informacoes_de_professor_cadastro
{
    char nome[20];
    int codigo;
} ProfessorCadastrado;

typedef struct informacoes_de_disciplina
{
    char nome[50];
    int codigo;
} Disciplina;

typedef struct node_disciplina
{
    Disciplina disciplina;
    struct node_disciplina *proximo;
    AlunoCadastrado alunos[50];
    ProfessorCadastrado professores[50];
} NodeDisciplina;

void inserirDisciplina(NodeDisciplina *head, char nome[], int codigo);
void exibirDisciplinas(NodeDisciplina *head);
bool existeDisciplina(int idDisciplina, NodeDisciplina *head);
char *buscaNomeDisciplinaEVincularAluno(NodeDisciplina *head, int idDisciplina, char nome[], int codigo);
char *buscaNomeDisciplinaEVincularProfessor(NodeDisciplina *head, int idDisciplina, char nome[], int codigo);
void exibirAlunosDeUmaDisciplina(NodeDisciplina *head, int idDisciplina);
void exibirProfessoresDeUmaDisciplina(NodeDisciplina *head, int idDisciplina);

#endif