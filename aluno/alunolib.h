#ifndef FUNCTIONS_H_INCLUDED
#define FUNCTIONS_H_INCLUDED
/* ^^ these are the include guards */
#include "../disciplina/disciplinalib.h"
typedef struct informacoes_de_aluno
{
    char nome[50];
    int ra;
    Disciplina disciplinas[10];
} Aluno;

typedef struct node_aluno
{
    Aluno aluno;
    struct node_aluno *proximo;
} NodeAluno;

void inserirAluno(NodeAluno *head, char nome[], int codigo);
void exibirAlunos(NodeAluno *head);
void vincularDisciplinaAoAluno(NodeAluno *headAluno, NodeDisciplina *headDisciplina, int idDisciplina, int idAluno);
bool existeAluno(int idAluno, NodeAluno *head);
void removerCadastroAlunoADisciplina(NodeAluno *headAluno, NodeDisciplina *headDisciplina, int idDisciplina, int idAluno);
void exibirAlunoPorId(NodeAluno *head, int idAluno);
#endif