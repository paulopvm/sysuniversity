#include "alunolib.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void inserirAluno(NodeAluno *head, char nome[], int codigo)
{
    NodeAluno *currentAluno = head;

    while (currentAluno->proximo != NULL)
    {
        currentAluno = currentAluno->proximo;
    }

    currentAluno->proximo = (NodeAluno *)malloc(sizeof(NodeAluno));
    strcpy(currentAluno->proximo->aluno.nome, nome);
    currentAluno->proximo->aluno.ra = codigo;
    for (int i = 0; i < 10; i++)
    {
        currentAluno->proximo->aluno.disciplinas[i].codigo = -1;
    }

    currentAluno->proximo->proximo = NULL;
}

void exibirAlunos(NodeAluno *head)
{
    NodeAluno *currentAluno = head;

    while (currentAluno != NULL)
    {
        currentAluno = currentAluno->proximo;
        if (currentAluno != NULL)
        {

            printf("NOME: %s RA: %d\n", currentAluno->aluno.nome, currentAluno->aluno.ra);
            printf("----------\n");
        }
    }
}

void vincularDisciplinaAoAluno(NodeAluno *headAluno, NodeDisciplina *headDisciplina, int idDisciplina, int idAluno)
{
    if (existeDisciplina(idDisciplina, headDisciplina) && existeAluno(idAluno, headAluno))
    {
        NodeAluno *currentAluno = headAluno;

        while (currentAluno != NULL)
        {
            if (currentAluno->aluno.ra == idAluno)
            {
                for (int i = 0; i < 10; i++)
                {
                    if (currentAluno->aluno.disciplinas[i].codigo == -1)
                    {

                        currentAluno->aluno.disciplinas[i].codigo = idDisciplina;
                        strcpy(currentAluno->aluno.disciplinas[i].nome, buscaNomeDisciplinaEVincularAluno(headDisciplina, idDisciplina, currentAluno->aluno.nome, currentAluno->aluno.ra));
                        break;
                    }
                }
            }
            currentAluno = currentAluno->proximo;
        }
    }
}

bool existeAluno(int idAluno, NodeAluno *head)
{
    NodeAluno *currentAluno = head;

    while (currentAluno != NULL)
    {
        if (currentAluno->aluno.ra == idAluno)
        {
            return true;
        }
        currentAluno = currentAluno->proximo;
    }
    printf("\tAluno não existente\n");
    return false;
}

void removerCadastroAlunoADisciplina(NodeAluno *headAluno, NodeDisciplina *headDisciplina, int idDisciplina, int idAluno)
{

    if (existeDisciplina(idDisciplina, headDisciplina) && existeAluno(idAluno, headAluno))
    {
        NodeAluno *currentAluno = headAluno;

        while (currentAluno != NULL)
        {
            if (currentAluno->aluno.ra == idAluno)
            {
                for (int i = 0; i < 10; i++)
                {
                    if (currentAluno->aluno.disciplinas[i].codigo == idDisciplina)
                    {
                        printf("\n\tDisciplina %d removida com sucesso\n", idDisciplina);
                        currentAluno->aluno.disciplinas[i].codigo = -1;
                        break;
                    }
                }
            }
            currentAluno = currentAluno->proximo;
        }
    }
}

void exibirAlunoPorId(NodeAluno *head, int idAluno)
{

    if (existeAluno(idAluno, head))
    {
        NodeAluno *currentAluno = head;

        while (currentAluno != NULL)
        {
            currentAluno = currentAluno->proximo;
            if (currentAluno != NULL)
            {
                if (currentAluno->aluno.ra == idAluno)
                {
                    printf("----------> INFORMAÇÔES ALUNO %s", currentAluno->aluno.nome);
                    printf("NOME: %sRA: %d\n", currentAluno->aluno.nome, currentAluno->aluno.ra);
                    printf("-----> Disciplinas vinculadas ao aluno %s\n", currentAluno->aluno.nome);

                    for (int i = 0; i < 10; i++)
                    {
                        if (currentAluno->aluno.disciplinas[i].codigo != -1)
                        {

                            printf("Disciplina: %s Código: %d\n", currentAluno->aluno.disciplinas[i].nome, currentAluno->aluno.disciplinas[i].codigo);
                            printf("----------\n");
                        }
                    }
                    break;
                }
            }
        }
    }
}