#include <stdio.h>
#include <stdlib.h>
#include "aluno/alunolib.h"
#include "disciplina/disciplinalib.h"
#include "professor/professorlib.h"

int main()
{
    Aluno aluno;
    Disciplina disciplina;
    Professor professor;

    NodeAluno *headAluno = NULL;
    headAluno = (NodeAluno *)malloc(sizeof(NodeAluno));

    NodeDisciplina *headDisciplina = NULL;
    headDisciplina = (NodeDisciplina *)malloc(sizeof(NodeDisciplina));

    NodeProfessor *headProfessor = NULL;
    headProfessor = (NodeProfessor *)malloc(sizeof(NodeProfessor));

    if (headAluno == NULL || headDisciplina == NULL || headProfessor == NULL)
    {
        return 1;
    };

    int menuOption;
    char cleanBuffer; // Variavel para limpar o buffer antes de ler a string depois de um inteiro
    while (menuOption != 0)
    {
        printf("--------------------------------Menu---------------------\n");
        printf("\t1 -  Cadastro de Alunos\n");
        printf("\t2 -  Cadastro de Disciplinas\n");
        printf("\t3 -  Cadastro de Professores\n\n");
        printf("\t4 -  Vincular disciplina ao aluno\n");
        printf("\t5 -  Vincular disciplina ao professor\n\n");
        printf("\t6 -  Remover aluno da disciplina\n");
        printf("\t7 -  Remover professor da disciplina\n\n");
        printf("\t8 -  Exibir aluno\n");
        printf("\t9 -  Exibir alunos, disciplinas e professores\n");
        printf("\t10 - Exibir professor\n");
        printf("\t11 - Exibir alunos de uma disciplina\n");
        printf("\t12 - Exibir professores de uma disciplina\n");
        printf("\t0 -  Finalizar programa\n");
        scanf("%d", &menuOption);
        fflush(stdin);

        switch (menuOption)
        {
        case 1:
            system("clear");
            printf("Digite o RA do aluno: ");
            scanf("%d", &aluno.ra);
            printf("Digite o nome do aluno: ");
            scanf("%c", &cleanBuffer); // Limpa o buffer para ler a string
            fgets(aluno.nome, 50, stdin);
            fflush(stdin);
            inserirAluno(headAluno, aluno.nome, aluno.ra);
            break;
        case 2:
            system("clear");
            printf("Digite o codigo da disciplina: ");
            scanf("%d", &disciplina.codigo);
            printf("Digite o nome da disciplina: ");
            scanf("%c", &cleanBuffer); // Limpa o buffer para ler a string
            fgets(disciplina.nome, 50, stdin);
            fflush(stdin);
            inserirDisciplina(headDisciplina, disciplina.nome, disciplina.codigo);
            break;
        case 3:
            system("clear");
            printf("Digite o codigo do professor: ");
            scanf("%d", &professor.codigo);
            printf("Digite o nome do professor: ");
            scanf("%c", &cleanBuffer); // Limpa o buffer para ler a string
            fgets(professor.nome, 50, stdin);
            fflush(stdin);
            inserirProfessor(headProfessor, professor.nome, professor.codigo);
            break;
        case 4:
            system("clear");
            printf("Insira o RA do aluno: ");
            scanf("%d", &aluno.ra);
            printf("Insira o código da disciplina para vincular ao aluno: ");
            scanf("%d", &disciplina.codigo);
            fflush(stdin);
            vincularDisciplinaAoAluno(headAluno, headDisciplina, disciplina.codigo, aluno.ra);
            break;
        case 5:
            system("clear");
            printf("Insira o código do professor: ");
            scanf("%d", &professor.codigo);
            printf("Insira o código da disciplina para vincular ao professor: ");
            scanf("%d", &disciplina.codigo);
            fflush(stdin);
            vincularDisciplinaAoProfessor(headProfessor, headDisciplina, disciplina.codigo, professor.codigo);
            break;
        case 6:
            system("clear");
            printf("Insira o código do aluno: ");
            scanf("%d", &aluno.ra);
            printf("Insira o código da disciplina que deseja remover do aluno: ");
            scanf("%d", &disciplina.codigo);
            fflush(stdin);
            removerCadastroAlunoADisciplina(headAluno, headDisciplina, disciplina.codigo, aluno.ra);
            break;
        case 7:
            system("clear");
            printf("Insira o código do professor: ");
            scanf("%d", &professor.codigo);
            printf("Insira o código da disciplina que deseja remover do professor: ");
            scanf("%d", &disciplina.codigo);
            fflush(stdin);
            removerCadastroProfessorADisciplina(headProfessor, headDisciplina, disciplina.codigo, professor.codigo);
            break;
        case 8:
            system("clear");
            printf("Insira o código do aluno: ");
            scanf("%d", &aluno.ra);
            fflush(stdin);
            exibirAlunoPorId(headAluno, aluno.ra);
            break;
        case 9:
            system("clear");
            printf("------>Alunos\n\n");
            exibirAlunos(headAluno);
            printf("\n");
            printf("------>Disciplinas\n\n");
            exibirDisciplinas(headDisciplina);
            printf("\n");
            printf("------>Professores\n\n");
            exibirProfessores(headProfessor);
            printf("\n");
            break;
        case 10:
            system("clear");
            printf("Insira o código do professor: ");
            scanf("%d", &professor.codigo);
            fflush(stdin);
            exibirProfessorPorCodigo(headProfessor, professor.codigo);
            break;
        case 11:
            system("clear");
            printf("Insira o código da disciplina: ");
            scanf("%d", &disciplina.codigo);
            fflush(stdin);
            exibirAlunosDeUmaDisciplina(headDisciplina, disciplina.codigo);
            break;
        case 12:
            system("clear");
            printf("Insira o código da disciplina: ");
            scanf("%d", &disciplina.codigo);
            fflush(stdin);
            exibirProfessoresDeUmaDisciplina(headDisciplina, disciplina.codigo);
            break;
        case 0:
            system("clear");
            printf("Finalizando programa...");
            fflush(stdin);
            return 1;
        default:
            printf("Opcao Invalida\n");
            break;
        }
    }

    return 0;
}
