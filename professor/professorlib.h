#ifndef PROFESSOR_H_INCLUDED
#define PROFESSOR_H_INCLUDED
/* ^^ these are the include guards */
#include "../disciplina/disciplinalib.h"

typedef struct informacoes_de_professor
{
    char nome[50];
    int codigo;
    Disciplina disciplinas[10];
} Professor;

typedef struct node_professor
{
    Professor professor;
    struct node_professor *proximo;
} NodeProfessor;

void inserirProfessor(NodeProfessor *head, char nome[], int codigo);
void exibirProfessores(NodeProfessor *head);
void vincularDisciplinaAoProfessor(NodeProfessor *headProfessor, NodeDisciplina *headDisciplina, int idDisciplina, int idProfessor);
bool existeProfessor(int idProfessor, NodeProfessor *head);
void removerCadastroProfessorADisciplina(NodeProfessor *headProfessor, NodeDisciplina *headDisciplina, int idDisciplina, int idProfessor);
void exibirProfessorPorCodigo(NodeProfessor *headProfessor, int codProfessor);
#endif