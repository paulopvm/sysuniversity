#include "professorlib.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void inserirProfessor(NodeProfessor *head, char nome[], int codigo)
{
    NodeProfessor *currentProfessor = head;
    while (currentProfessor->proximo != NULL)
    {
        currentProfessor = currentProfessor->proximo;
    }

    currentProfessor->proximo = (NodeProfessor *)malloc(sizeof(NodeProfessor));
    strcpy(currentProfessor->proximo->professor.nome, nome);
    currentProfessor->proximo->professor.codigo = codigo;
    for (int i = 0; i < 10; i++)
    {
        currentProfessor->proximo->professor.disciplinas[i].codigo = -1;
    }
    currentProfessor->proximo->proximo = NULL;
}

void exibirProfessores(NodeProfessor *head)
{
    NodeProfessor *currentProfessor = head;

    while (currentProfessor != NULL)
    {
        currentProfessor = currentProfessor->proximo;
        if (currentProfessor != NULL)
        {
            printf("NOME: %s CÓDIGO: %d\n", currentProfessor->professor.nome, currentProfessor->professor.codigo);
            printf("----------\n");
        }
    }
}

void vincularDisciplinaAoProfessor(NodeProfessor *headProfessor, NodeDisciplina *headDisciplina, int idDisciplina, int idProfessor)
{
    if (existeDisciplina(idDisciplina, headDisciplina) && existeProfessor(idProfessor, headProfessor))
    {
        NodeProfessor *currentProfessor = headProfessor;

        while (currentProfessor != NULL)
        {
            if (currentProfessor->professor.codigo == idProfessor)
            {
                for (int i = 0; i < 10; i++)
                {
                    if (currentProfessor->professor.disciplinas[i].codigo == -1)
                    {

                        currentProfessor->professor.disciplinas[i].codigo = idDisciplina;
                        strcpy(currentProfessor->professor.disciplinas[i].nome, buscaNomeDisciplinaEVincularProfessor(headDisciplina, idDisciplina, currentProfessor->professor.nome, currentProfessor->professor.codigo));
                        break;
                    }
                }
            }
            currentProfessor = currentProfessor->proximo;
        }
    }
}

bool existeProfessor(int idProfessor, NodeProfessor *head)
{
    NodeProfessor *currentProfessor = head;

    while (currentProfessor != NULL)
    {
        if (currentProfessor->professor.codigo == idProfessor)
        {
            return true;
        }
        currentProfessor = currentProfessor->proximo;
    }
    printf("\tProfessor não existente\n");
    return false;
}

void removerCadastroProfessorADisciplina(NodeProfessor *headProfessor, NodeDisciplina *headDisciplina, int idDisciplina, int idProfessor)
{

    if (existeDisciplina(idDisciplina, headDisciplina) && existeProfessor(idProfessor, headProfessor))
    {
        NodeProfessor *currentProfessor = headProfessor;

        while (currentProfessor != NULL)
        {
            if (currentProfessor->professor.codigo == idProfessor)
            {
                for (int i = 0; i < 10; i++)
                {
                    if (currentProfessor->professor.disciplinas[i].codigo == idDisciplina)
                    {
                        printf("\n\tDisciplina %d removida com sucesso\n", idDisciplina);
                        currentProfessor->professor.disciplinas[i].codigo = -1;
                        break;
                    }
                }
            }
            currentProfessor = currentProfessor->proximo;
        }
    }
}

void exibirProfessorPorCodigo(NodeProfessor *headProfessor, int codProfessor)
{

    if (existeProfessor(codProfessor, headProfessor))
    {

        NodeProfessor *currentProfessor = headProfessor;

        while (currentProfessor != NULL)
        {

            currentProfessor = currentProfessor->proximo;
            if (codProfessor == currentProfessor->professor.codigo)
            {

                printf("----------> INFORMAÇÔES PROFESSOR %s", currentProfessor->professor.nome);
                printf("NOME: %sCODIGO: %d\n", currentProfessor->professor.nome, currentProfessor->professor.codigo);

                if (currentProfessor->professor.disciplinas[0].codigo != -1)
                    printf("DISCIPLINAS LECIONADAS \n\n");
                for (int i = 0; i < 10; i++)
                {

                    if (currentProfessor->professor.disciplinas[i].codigo != -1)
                        printf("NOME: %sCODIGO: %d\n\n", currentProfessor->professor.disciplinas[i].nome, currentProfessor->professor.disciplinas[i].codigo);
                }
                break;
            }
        }
    }
}
