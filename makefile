
CC = gcc
CFLAGS = -Wall -g
LDFLAGS =
OBJFILES = main.o aluno/alunolib.o disciplina/disciplinalib.o professor/professorlib.o
TARGET = sysUniversity.out

all: $(TARGET)

$(TARGET): $(OBJFILES)
	$(CC) $(CFLAGS) -o $(TARGET) $(OBJFILES) $(LDFLAGS)

clean:
	rm -f $(OBJFILES) $(TARGET) *~